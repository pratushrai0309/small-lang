@{%
const myLexer = require("./lexer");
%}

@lexer myLexer

#Support For Next Line
statements
        ->statements %NL statement {% 
                (data) => {
                        return [...data[0], data[2]]
                }
        %} | statement{% 
                (data) =>{
                        return [data[0]]
                }
         %}

statement
    -> var_assign{% id %}
    |  fun_call {% id %}
    
var_assign
    -> %identifier _ %assign _ expr {% 
    (data) => {
        return{ 
                type: "var_assign",
                var_name: data[0],
                value: data[4]
        }
    }
     %}

fun_call 
        -> %identifier _ "(" _ arg_list _ ")" 
        {% 
        (data) => {
                return{
                        type: "fun_call",
                        fun_name: data[0],
                        arguments: data[4]
                }
        }
         %} 

arg_list
        -> expr
        {% 
        (data) => {
                return [data[0]]
        }
         %} 
        | null {% 
        (data)=> {
                return []
        }
         %}
        |  arg_list __ expr {%
        (data) => {
                return [...data[0], data[2]]
        }
         %}

expr
    -> %string     {% id %}
    |  %number     {% id %}
    |  %identifier {% id %}
    |  boolean     {% id %}

boolean -> %true | %false
# Optional whitespace    
_ -> %WS:*

# Mandatory whitespace
__ -> %WS:+